const express = require('express');
const app = express();
const PORT = 3000;

// GET endpoint - http://127.0.0.1:3000/
app.get('/', (req, res) => {
    res.send('Hello world!\n');
});

/**
 * This function multiplies multiplicant with multiplier.
 * @param {number} multiplicant first param
 * @param {number} multiplier second param
 * @returns {number} product
 */
const multiply = (multiplicant, multiplier) => {
    const product = multiplicant * multiplier;
    return product;
};

// GET multiply endpoint - http://127.0.0.1:3000/multiply?a=3&b=5
app.get('/multiply', (req, res) => {
    try {
        const multiplicant = parseFloat(req.query.a);
        const multiplier = parseFloat(req.query.b);
        if (isNaN(multiplicant)) throw new Error('Invalid multiplicant value');
        if (isNaN(multiplier)) throw new Error('Invalid multiplier value');
        const product = multiply(multiplicant, multiplier);
        console.log({ multiplicant, multiplier, product });
        res.send(product.toString(10) + '\n');
    } catch (err) {
        console.error(err.message);
        res.send("Couldn't calculate the product. Try again.\n")
    }
});

app.listen(PORT, () => console.log(
    `Listening at http://127.0.0.1:${PORT}`
));